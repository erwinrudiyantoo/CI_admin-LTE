-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2017 at 02:14 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yury_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `bhn_id` varchar(10) NOT NULL,
  `bhn_nama` varchar(40) NOT NULL,
  `bhn_kalori` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`bhn_id`, `bhn_nama`, `bhn_kalori`) VALUES
('BHN-0001', '11', 111),
('BHN-0002', 'aa', 11);

-- --------------------------------------------------------

--
-- Table structure for table `bahan_makanan`
--

CREATE TABLE `bahan_makanan` (
  `bma_bhn_id` varchar(10) NOT NULL,
  `bma_mkn_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `mkn_id` varchar(10) NOT NULL,
  `mkn_nama` varchar(40) NOT NULL,
  `mkn_tipe` varchar(7) NOT NULL,
  `mkn_kalori` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `men_id` varchar(10) NOT NULL,
  `men_nama` varchar(40) NOT NULL,
  `men_kal` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_makanan`
--

CREATE TABLE `menu_makanan` (
  `mma_men_id` varchar(10) NOT NULL,
  `mma_mkn_id` varchar(10) NOT NULL,
  `mma_mnm_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `minuman`
--

CREATE TABLE `minuman` (
  `mnm_id` varchar(10) NOT NULL,
  `mnm_nama` varchar(40) NOT NULL,
  `mnm_kalori` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `pak_id` varchar(10) NOT NULL,
  `pak_nama` varchar(40) DEFAULT NULL,
  `pak_durasi` smallint(2) DEFAULT NULL,
  `pak_harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`pak_id`, `pak_nama`, `pak_durasi`, `pak_harga`) VALUES
('PKT-0001', 'asd', 1, 12),
('PKT-0002', 'asd', 0, 111);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `pel_id` varchar(10) NOT NULL,
  `pel_status` smallint(1) DEFAULT '0',
  `pel_masaaktif` date DEFAULT NULL,
  `pel_username` varchar(10) NOT NULL,
  `pel_nama` varchar(50) NOT NULL,
  `pel_tanggal` date NOT NULL,
  `pel_tempat` varchar(40) NOT NULL,
  `pel_email` varchar(40) NOT NULL,
  `pel_kelamin` varchar(12) NOT NULL,
  `pel_password` varchar(20) NOT NULL,
  `pel_nomor` varchar(20) NOT NULL,
  `pel_jawab1` varchar(100) NOT NULL,
  `pel_jawab` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_paket`
--

CREATE TABLE `transaksi_paket` (
  `tpa_id` varchar(10) NOT NULL,
  `tpa_tgl` date NOT NULL,
  `tpa_total` int(11) NOT NULL,
  `tpa_user` varchar(10) NOT NULL,
  `tpa_paket` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `usr_id` varchar(10) NOT NULL,
  `usr_nama` varchar(50) NOT NULL,
  `usr_tanggal` date NOT NULL,
  `usr_tempat` varchar(40) NOT NULL,
  `usr_email` varchar(40) NOT NULL,
  `usr_jkelamin` varchar(11) NOT NULL,
  `usr_password` varchar(20) NOT NULL,
  `usr_role` smallint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`bhn_id`);

--
-- Indexes for table `bahan_makanan`
--
ALTER TABLE `bahan_makanan`
  ADD PRIMARY KEY (`bma_bhn_id`,`bma_mkn_id`),
  ADD KEY `fk_mkn_id` (`bma_mkn_id`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`mkn_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`men_id`);

--
-- Indexes for table `menu_makanan`
--
ALTER TABLE `menu_makanan`
  ADD PRIMARY KEY (`mma_men_id`,`mma_mkn_id`,`mma_mnm_id`),
  ADD KEY `fk_men_mkn_id` (`mma_mkn_id`),
  ADD KEY `fk_men_mnm_id` (`mma_mnm_id`);

--
-- Indexes for table `minuman`
--
ALTER TABLE `minuman`
  ADD PRIMARY KEY (`mnm_id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`pak_id`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`pel_id`);

--
-- Indexes for table `transaksi_paket`
--
ALTER TABLE `transaksi_paket`
  ADD PRIMARY KEY (`tpa_id`),
  ADD KEY `fk_tra_usr_id` (`tpa_user`),
  ADD KEY `fk_tra_pkt_id` (`tpa_paket`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bahan_makanan`
--
ALTER TABLE `bahan_makanan`
  ADD CONSTRAINT `fk_bhn_id` FOREIGN KEY (`bma_bhn_id`) REFERENCES `bahan` (`bhn_id`),
  ADD CONSTRAINT `fk_mkn_id` FOREIGN KEY (`bma_mkn_id`) REFERENCES `makanan` (`mkn_id`);

--
-- Constraints for table `menu_makanan`
--
ALTER TABLE `menu_makanan`
  ADD CONSTRAINT `fk_men_men_id` FOREIGN KEY (`mma_men_id`) REFERENCES `menu` (`men_id`),
  ADD CONSTRAINT `fk_men_mkn_id` FOREIGN KEY (`mma_mkn_id`) REFERENCES `makanan` (`mkn_id`),
  ADD CONSTRAINT `fk_men_mnm_id` FOREIGN KEY (`mma_mnm_id`) REFERENCES `minuman` (`mnm_id`);

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `fk_usr_id` FOREIGN KEY (`pel_id`) REFERENCES `user` (`usr_id`);

--
-- Constraints for table `transaksi_paket`
--
ALTER TABLE `transaksi_paket`
  ADD CONSTRAINT `fk_tra_pkt_id` FOREIGN KEY (`tpa_paket`) REFERENCES `paket` (`pak_id`),
  ADD CONSTRAINT `fk_tra_usr_id` FOREIGN KEY (`tpa_user`) REFERENCES `user` (`usr_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
