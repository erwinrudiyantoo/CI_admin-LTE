<?php 
	//membuat class baru inherit CI_Model
	class MinumanModel extends CI_Model
	{
		//fungsi untuk melakukan penambahan data pada database
		function tambah()
		{
			//mengambil pak_id , pak_nama ,pak_durasi dan pak_harga dari View
			//lalu diletakan pada variable $pak_id , $pak_nama ,$pak_durasi dan $pak_harga
			$mnm_id = $this ->input->post('mnm_id');
			$mnm_nama = $this ->input->post('mnm_nama');
			$mnm_kalori = $this ->input->post('mnm_kalori');
			
			//meletakan isi dari variable $nama , $email , dan $pesan dalam array
			//'nama' , 'email' , 'pesan' adalah nama kolom di table pada database
			$data = array('mnm_id' => $mnm_id , 'mnm_nama' => $mnm_nama , 'mnm_kalori' => $mnm_kalori );
			
			//menginput array $data ke dalam tabel komentar pada database
			$this->db->insert('minuman',$data);
		}
		//fungsi untuk membaca data dari database
		function tampil()
		{
			//mengambil data dari table komentar di DB
			//diletakan pada variable $tampil
			
			$tampil = $this->db->get('minuman');
			
			//memeriksa jumlah row yang ditemukan pada tabel komentar
			if($tampil->num_rows() > 0)
			{
				//perulangan untuk setiap data yang ditemukan 
				//akan diletakan pada variable $data
				foreach($tampil->result() as $data)
				{
					//setiap data yang ditemukan diletakan pada array
					$hasil[] = $data;
				}
				//mengembailikan nilai data komentar pada array $hasil
				return $hasil;
				
			}
		}
		function hapus($mnm_id)
		{
			//menghapus data pada database di tabel komentar
			//dengan id sesuai dengan isi data pada variabel id
			$this->db->delete('minuman',array('mnm_id'=>$mnm_id));
			
			//mengarahkan file ke controller komentar
			//artinya mengarahkan ke komentar/index
			redirect('MinumanController');
		}
		function ubah_tampil($mnm_id)
		{
			//membaca data pada table komentar, sesuai dengan id yang dikirimkan
			return $this->db->get_where('minuman', array('mnm_id'=>$mnm_id))->row();
		}
		
		function ubah($mnm_id)
		{
			//mengambil nama, email, dan pesan dari view lalu diletakkan di variable dibawah ini
			$mnm_id = $this ->input->post('mnm_id');
			$mnm_nama = $this ->input->post('mnm_nama');
			$mnm_kalori = $this ->input->post('mnm_kalori');
			
			//meletakkan isi dari variabel $nama, $email, $pesan dalam array
			//'nama', 'email', 'pesan' adalah nama kolom di tabel pada database
			$data = array('mnm_nama' => $mnm_nama , 'mnm_kalori' => $mnm_kalori );
			
			//memberikan kondisi bahwa id yang diubah pada database adalah id yang diberikan pada variabel id
			$this->db->where('mnm_id',$mnm_id);
			
			//mengupdate table komentar sesuai isian array data dan parameter id
			$this->db->update('minuman',$data);
		}
		function idauto()
		{
			$hasila = $this->db->get('minuman');
			if($hasila->num_rows() > 0)
			{
				foreach($hasila->result() as $data)
				{
					$tampil[] = $data;
				}
				return $tampil;
			}
		}
	}
?>
			