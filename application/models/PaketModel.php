<?php 
	//membuat class baru inherit CI_Model
	class PaketModel extends CI_Model
	{
		//fungsi untuk melakukan penambahan data pada database
		function tambah()
		{
			//mengambil pak_id , pak_nama ,pak_durasi dan pak_harga dari View
			//lalu diletakan pada variable $pak_id , $pak_nama ,$pak_durasi dan $pak_harga
			$pak_id = $this ->input->post('pak_id');
			$pak_nama = $this ->input->post('pak_nama');
			$pak_durasi = $this ->input->post('pak_durasi');
			$pak_harga = $this ->input->post('pak_harga');
			
			//meletakan isi dari variable $nama , $email , dan $pesan dalam array
			//'nama' , 'email' , 'pesan' adalah nama kolom di table pada database
			$data = array('pak_id' => $pak_id , 'pak_nama' => $pak_nama , 'pak_durasi' => $pak_durasi , 'pak_harga' => $pak_harga);
			
			//menginput array $data ke dalam tabel komentar pada database
			$this->db->insert('paket',$data);
		}
		//fungsi untuk membaca data dari database
		function tampil()
		{
			//mengambil data dari table komentar di DB
			//diletakan pada variable $tampil
			
			$tampil = $this->db->get('paket');
			
			//memeriksa jumlah row yang ditemukan pada tabel komentar
			if($tampil->num_rows() > 0)
			{
				//perulangan untuk setiap data yang ditemukan 
				//akan diletakan pada variable $data
				foreach($tampil->result() as $data)
				{
					//setiap data yang ditemukan diletakan pada array
					$hasil[] = $data;
				}
				//mengembailikan nilai data komentar pada array $hasil
				return $hasil;
				
			}
		}
		function hapus($pak_id)
		{
			//menghapus data pada database di tabel komentar
			//dengan id sesuai dengan isi data pada variabel id
			$this->db->delete('paket',array('pak_id'=>$pak_id));
			
			//mengarahkan file ke controller komentar
			//artinya mengarahkan ke komentar/index
			redirect('PaketController');
		}
		function ubah_tampil($pak_id)
		{
			//membaca data pada table komentar, sesuai dengan id yang dikirimkan
			return $this->db->get_where('paket', array('pak_id'=>$pak_id))->row();
		}
		
		function ubah($pak_id)
		{
			//mengambil nama, email, dan pesan dari view lalu diletakkan di variable dibawah ini
			$pak_id = $this ->input->post('pak_id');
			$pak_nama = $this ->input->post('pak_nama');
			$pak_durasi = $this ->input->post('pak_durasi');
			$pak_harga = $this ->input->post('pak_harga');
			
			//meletakkan isi dari variabel $nama, $email, $pesan dalam array
			//'nama', 'email', 'pesan' adalah nama kolom di tabel pada database
			$data = array('pak_id' => $pak_id , 'pak_nama' => $pak_nama , 'pak_durasi' => $pak_durasi , 'pak_harga' => $pak_harga);
			
			//memberikan kondisi bahwa id yang diubah pada database adalah id yang diberikan pada variabel id
			$this->db->where('pak_id',$pak_id);
			
			//mengupdate table komentar sesuai isian array data dan parameter id
			$this->db->update('paket',$data);
		}
		function idauto()
		{
			$hasila = $this->db->get('paket');
			if($hasila->num_rows() > 0)
			{
				foreach($hasila->result() as $data)
				{
					$tampil[] = $data;
				}
				return $tampil;
			}
		}
	}
?>
			