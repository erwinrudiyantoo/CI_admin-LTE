<?php

	class MakananController extends CI_Controller
	{
		//fungsi untuk menambahkan data
		function tambah()
		{
			//jika ada post submit yang diterima pada form
			if($this->input->post('submit'))
			{
				//meload file model komentar
				$this->load->model('MakananModel');
				
				//menjalankan fungsi tambah pada model
				$this->MakananModel->tambah();
				echo "<script>
				alert('Data berhasil ditambahkan!');
				window.location.href='index';
				</script>";
				//menjalankan file ke controller komentar
				//artinya mengarah ke komentar/index
				//redirect('MakananController');
			}
			
			//meload tampilan komentar tambah
			$this->load->model('MakananModel');
			$data['tampil'] = $this->MakananModel->idauto();
			//meload tampilan komentar tambah
			$this->load->view('makanan_tambah', $data);

		}
		function index()
		{
			//meload file model komentar
			$this->load->model('MakananModel');
			
			//mengambil nilai pengembalian dari fungsi tampil pada model 
			//return yang didapat berupa array dari $hasil
			$data['hasil'] = $this->MakananModel->tampil();
			
			//meload file view komentar_tampil
			//sekaligus memberikan parameter $data
			//yang berisi data $hasil dari fungsi tampil pada model
			$this->load->view('makanan_tampil',$data);
		}
		function delete($mkn_id)
		{
			$this->load->model('MakananModel');
			$this->MakananModel->hapus($mkn_id);
			redirect('MakananController');
		}
		function update($mkn_id)
		{
			if($_POST==null)
			{
				//meload file model komentar
				$this->load->model('MakananModel');
				
				//menjalankan fungsi ubah_tampil pada model
				$data['hasil']=$this->MakananModel->ubah_tampil($mkn_id);
				
				//meload file view komentar_ubah dengan parameter array $data 
				$this->load->view('makanan_ubah',$data);
			}
			else
			{
				//meload file model komentar
				$this->load->model('MakananModel');
				
				//menjalankan fungsi ubah pada model
				$this->MakananModel->ubah($id);
				
				//mengarahkan file ke controller komentar
				//artinya mengarahkan ke komentar/index
				echo "<script>
				alert('Data berhasil diubah!');
				window.location.href='index';
				</script>";
			}
		}
	}

?>