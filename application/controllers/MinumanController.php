<?php

	class MinumanController extends CI_Controller
	{
		//fungsi untuk menambahkan data
		function tambah()
		{
			//jika ada post submit yang diterima pada form
			if($this->input->post('submit'))
			{
				//meload file model komentar
				$this->load->model('MinumanModel');
				
				//menjalankan fungsi tambah pada model
				$this->MinumanModel->tambah();
				echo "<script>
				alert('Data berhasil ditambahkan!');
				window.location.href='index';
				</script>";
				//menjalankan file ke controller komentar
				//artinya mengarah ke komentar/index
				//redirect('MinumanController');
			}
			
			//meload tampilan komentar tambah
			$this->load->model('MinumanModel');
			$data['tampil'] = $this->MinumanModel->idauto();
			//meload tampilan komentar tambah
			$this->load->view('minuman_tambah', $data);

		}
		function index()
		{
			//meload file model komentar
			$this->load->model('MinumanModel');
			
			//mengambil nilai pengembalian dari fungsi tampil pada model 
			//return yang didapat berupa array dari $hasil
			$data['hasil'] = $this->MinumanModel->tampil();
			
			//meload file view komentar_tampil
			//sekaligus memberikan parameter $data
			//yang berisi data $hasil dari fungsi tampil pada model
			$this->load->view('minuman_tampil',$data);
		}
		function delete($mnm_id)
		{
			$this->load->model('MinumanModel');
			$this->MinumanModel->hapus($mnm_id);
			redirect('MinumanController');
		}
		function update($mnm_id)
		{
			if($_POST==null)
			{
				//meload file model komentar
				$this->load->model('MinumanModel');
				
				//menjalankan fungsi ubah_tampil pada model
				$data['hasil']=$this->MinumanModel->ubah_tampil($mnm_id);
				
				//meload file view komentar_ubah dengan parameter array $data 
				$this->load->view('minuman_ubah',$data);
			}
			else
			{
				//meload file model komentar
				$this->load->model('MinumanModel');
				
				//menjalankan fungsi ubah pada model
				$this->MinumanModel->ubah($id);
				
				//mengarahkan file ke controller komentar
				//artinya mengarahkan ke komentar/index
				echo "<script>
				alert('Data berhasil diubah!');
				window.location.href='index';
				</script>";
			}
		}
	}

?>