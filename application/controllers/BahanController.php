<?php

	class BahanController extends CI_Controller
	{
		//fungsi untuk menambahkan data
		function tambah()
		{
			//jika ada post submit yang diterima pada form
			if($this->input->post('submit'))
			{
				//meload file model komentar
				$this->load->model('BahanModel');
				
				//menjalankan fungsi tambah pada model
				$this->BahanModel->tambah();
				echo "<script>
				alert('Data berhasil ditambahkan!');
				window.location.href='index';
				</script>";
				//menjalankan file ke controller komentar
				//artinya mengarah ke komentar/index
				//redirect('BahanController');
			}
			$this->load->model('BahanModel');
			$data['tampil'] = $this->BahanModel->idauto();
			//meload tampilan komentar tambah
			$this->load->view('bahan_tambah', $data);
		}
		function index()
		{
			//meload file model komentar
			$this->load->model('BahanModel');
			
			//mengambil nilai pengembalian dari fungsi tampil pada model 
			//return yang didapat berupa array dari $hasil
			$data['hasil'] = $this->BahanModel->tampil();
			
			//meload file view komentar_tampil
			//sekaligus memberikan parameter $data
			//yang berisi data $hasil dari fungsi tampil pada model
			$this->load->view('bahan_tampil',$data);
		}
		function delete($mnm_id)
		{
			$this->load->model('BahanModel');
			$this->BahanModel->hapus($mnm_id);
			redirect('BahanController');
		}
		function update($mnm_id)
		{
			if($_POST==null)
			{
				//meload file model komentar
				$this->load->model('BahanModel');
				
				//menjalankan fungsi ubah_tampil pada model
				$data['hasil']=$this->BahanModel->ubah_tampil($mnm_id);
				
				//meload file view komentar_ubah dengan parameter array $data 
				$this->load->view('bahan_ubah',$data);
			}
			else
			{
				//meload file model komentar
				$this->load->model('BahanModel');
				
				//menjalankan fungsi ubah pada model
				$this->BahanModel->ubah($mnm_id);
				echo "<script>
				alert('Data berhasil diubah!');
				window.location.href='../index';
				</script>";
				//mengarahkan file ke controller komentar
				//artinya mengarahkan ke komentar/index
				//redirect('BahanController');
			}
		}
	}

?>