<?php

	class PaketController extends CI_Controller
	{
		//fungsi untuk menambahkan data
		function tambah()
		{
			//jika ada post submit yang diterima pada form
			if($this->input->post('submit'))
			{
				//meload file model komentar
				$this->load->model('PaketModel');
				
				//menjalankan fungsi tambah pada model
				$this->PaketModel->tambah();
				
				//menjalankan file ke controller komentar
				//artinya mengarah ke komentar/index
				echo "<script>
				alert('Data berhasil ditambahkan!');
				window.location.href='index';
				</script>";
			}
			
			//meload tampilan komentar tambah
			$this->load->model('PaketModel');
			$data['tampil'] = $this->PaketModel->idauto();
			$this->load->view('paket_tambah', $data);
		}
		function index()
		{
			//meload file model komentar
			$this->load->model('PaketModel');
			
			//mengambil nilai pengembalian dari fungsi tampil pada model 
			//return yang didapat berupa array dari $hasil
			$data['hasil'] = $this->PaketModel->tampil();
			
			//meload file view komentar_tampil
			//sekaligus memberikan parameter $data
			//yang berisi data $hasil dari fungsi tampil pada model
			$this->load->view('Paket_tampil',$data);
		}
		function delete($pak_id)
		{
			$this->load->model('PaketModel');
			$this->PaketModel->hapus($pak_id);
			redirect('PaketController');
		}
		function update($pak_id)
		{
			if($_POST==null)
			{
				//meload file model komentar
				$this->load->model('PaketModel');
				
				//menjalankan fungsi ubah_tampil pada model
				$data['hasil']=$this->PaketModel->ubah_tampil($pak_id);
				
				//meload file view komentar_ubah dengan parameter array $data 
				$this->load->view('paket_ubah',$data);
			}
			else
			{
				//meload file model komentar
				$this->load->model('PaketModel');
				
				//menjalankan fungsi ubah pada model
				$this->PaketModel->ubah($id);
				
				//mengarahkan file ke controller komentar
				//artinya mengarahkan ke komentar/index
				echo "<script>
				alert('Data berhasil diubah!');
				window.location.href='index';
				</script>";
			}
		}
	}

?>