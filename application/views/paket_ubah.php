<script language='javascript'>
function validAngka(a)
{
	if(!/^[0-9.]+$/.test(a.value))
	{
	a.value = a.value.substring(0,a.value.length-1000);
	}
}
</script>
<html>
	<head>
		<title>Ubah Paket</title>
	</head>
	<body>
		<h3>Form Ubah Paket</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('PaketController/update/'.$hasil->pak_id);
		?>
		<table>
			<tr>
				<td>ID Paket</td>
				<td>:</td>
				<td>
					<?php
						$pak_id = array('readonly'=>'true' ,'name' => 'pak_id'  , 'maxlength' => '30' ,
							'size' => '30' ,'value'=>$hasil->pak_id , 'readonly'=>'true');
							echo form_input($pak_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Paket</td>
				<td>:</td>
				<td>
					<?php
						$pak_nama = array('name' => 'pak_nama' , 'maxlength' => '30' ,
							'value' => '' , 'size' => '20', 'value'=>$hasil->pak_nama);
							echo form_input($pak_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Paket Durasi</td>
				<td>:</td>
				<td>
					<?php
						$pak_durasi = array('name' => 'pak_durasi' , 'cols' => '30' , 'value'=>$hasil->pak_durasi , 'rows' => '3');
							echo form_input($pak_durasi);
					?></td>
					<td>Hari</td>
			</tr>
			<tr>
				<td>Paket Harga</td>
				<td>:</td>
				<td>
					<?php
						$pak_durasi = array('onkeypress' => 'validAngka($this)' ,'name' => 'pak_harga' , 'value'=>$hasil->pak_harga , 'cols' => '30' , 'rows' => '3');
							echo form_input( $pak_durasi);
					?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'Ubah' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>