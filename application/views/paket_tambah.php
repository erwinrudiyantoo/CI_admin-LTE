<script language='javascript'>
function validAngka(a)
{
	if(!/^[0-9.]+$/.test(a.value))
	{
	a.value = a.value.substring(0,a.value.length-1000);
	}
}
</script>
<html>
	<?php 
          if(empty($tampil))
          {
            $idauto = "PKT-0001";
          }
          else
          {
            foreach ($tampil as $data) 
            {
              list($huruf, $angka) = explode('-', $data->pak_id);
              $angka = $angka + 1;
              if($angka<10)
              {
                $idauto = $huruf.'-000'.$angka;
              }
              else if($angka<100)
              {
                $idauto = $huruf.'-00'.$angka;
              }
              else if($angka<1000)
              {
                $idauto = $huruf.'-0'.$angka;
              }
              else if($angka<10000)
              {
                $idauto = $huruf.'-'.$angka;
              }
            }  
          }
    ?>
	<head>
		<title>Tambah Paket</title>
	</head>
	<body>
		<h3>Form Tambah Paket</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('PaketController/tambah');
		?>
		<table>
			<tr>
				<td>ID Paket</td>
				<td>:</td>
				<td>
					<?php
						$pak_id = array('name' => 'pak_id' , 'maxlength' => '30' ,
							'value' => $idauto , 'size' => '30' , 'readonly'=>'true');
							echo form_input($pak_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Paket</td>
				<td>:</td>
				<td>
					<?php
						$pak_nama = array('name' => 'pak_nama' , 'maxlength' => '30' ,
							'value' => '' , 'size' => '20');
							echo form_input($pak_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Paket Durasi</td>
				<td>:</td>
				<td>
					<?php
						$pak_durasi = array('name' => 'pak_durasi' , 'cols' => '30' , 'rows' => '3');
							echo form_input($pak_durasi);
					?></td>
					<td>Hari</td>
			</tr>
			<tr>
				<td>Paket Harga</td>
				<td>:</td>
				<td>
					<?php
						$pak_durasi = array('onkeypress' => 'validAngka($this)' ,'name' => 'pak_harga' , 'cols' => '30' , 'rows' => '3');
							echo form_input( $pak_durasi);
					?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'simpan' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>