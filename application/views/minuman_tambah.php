<script language='javascript'>
function validAngka(a)
{
	if(!/^[0-9.]+$/.test(a.value))
	{
	a.value = a.value.substring(0,a.value.length-1000);
	}
}
</script>
<html>
	<head>
		<title>Tambah minuman</title>
	</head>
	<?php 
          if(empty($tampil))
          {
            $idauto = "MUM-0001";
          }
          else
          {
            foreach ($tampil as $data) 
            {
              list($huruf, $angka) = explode('-', $data->mnm_id);
              $angka = $angka + 1;
              if($angka<10)
              {
                $idauto = $huruf.'-000'.$angka;
              }
              else if($angka<100)
              {
                $idauto = $huruf.'-00'.$angka;
              }
              else if($angka<1000)
              {
                $idauto = $huruf.'-0'.$angka;
              }
              else if($angka<10000)
              {
                $idauto = $huruf.'-'.$angka;
              }
            }  
          }
         ?>
	<body>
		<h3>Form Tambah minuman</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('MinumanController/tambah');
		?>
		<table>
			<tr>
				<td>ID Minuman</td>
				<td>:</td>
				<td>
					<?php
						$mnm_id = array('readonly'=>'true' ,'name' => 'mnm_id' , 'maxlength' => '30' ,
							'value' => $idauto , 'size' => '30');
							echo form_input($mnm_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Minuman</td>
				<td>:</td>
				<td>
					<?php
						$mnm_nama = array('name' => 'mnm_nama' , 'maxlength' => '30' ,
							'value' => '' , 'size' => '20');
							echo form_input($mnm_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Kalori</td>
				<td>:</td>
				<td>
					<?php
						$mnm_kalori = array('name' => 'mnm_kalori' , 'cols' => '30' , 'rows' => '3');
							echo form_input($mnm_kalori);
					?></td>
					
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'simpan' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>