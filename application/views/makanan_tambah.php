
<html>
	<?php 
          if(empty($tampil))
          {
            $idauto = "MAM-0001";
          }
          else
          {
            foreach ($tampil as $data) 
            {
              list($huruf, $angka) = explode('-', $data->mkn_id);
              $angka = $angka + 1;
              if($angka<10)
              {
                $idauto = $huruf.'-000'.$angka;
              }
              else if($angka<100)
              {
                $idauto = $huruf.'-00'.$angka;
              }
              else if($angka<1000)
              {
                $idauto = $huruf.'-0'.$angka;
              }
              else if($angka<10000)
              {
                $idauto = $huruf.'-'.$angka;
              }
            }  
          }
    ?>
	<head>
		<title>Tambah Menu Makanan</title>
	</head>
	<body>
		<h3>Form Menu Makanan</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('MakananController/tambah');
		?>
		<table>
			<tr>
				<td>ID Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_id = array('name' => 'mkn_id' , 'maxlength' => '30' ,
							'value' => $idauto , 'size' => '30' , 'readonly'=>'true');
							echo form_input($mkn_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_nama = array('name' => 'mkn_nama' , 'maxlength' => '30' ,
							'value' => '' , 'size' => '20');
							echo form_input($mkn_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Tipe Makanan</td>
				<td>:</td>
				<td>
					<?php 
          						// $role = array('name'=>'role', 'maxlength'=>'20', 'value'=>'', 'size'=>'20', 'class' => 'form-control');
          						// echo form_input($role);
                      $mkn_tipe = array(
                      '' => '-- Pilih Tipe Makanan --',
                      'Nabati' => 'Nabati',
                      'Hewani' => 'Hewani'
                      );
                      echo form_dropdown('role', $mkn_tipe, '-- Pilih Tipe Makanan --');
          			?>
				</td>
			</tr>
			<tr>
				<td>Kalori Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_durasi = array('onkeypress' => 'validAngka($this)' ,'name' => 'mkn_harga' , 'cols' => '30' , 'rows' => '3');
							echo form_input( $mkn_durasi);
					?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'simpan' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>