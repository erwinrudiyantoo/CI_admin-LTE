
<html>
	
	<head>
		<title>Ubah Menu Makanan</title>
	</head>
	<body>
		<h3>Form Menu Makanan</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('MakananController/tambah'.$hasil->mkn_id);
		?>
		<table>
			<tr>
				<td>ID Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_id = array('name' => 'mkn_id' , 'maxlength' => '30' ,
							'value' => $hasil->mkn_id , 'size' => '30' , 'readonly'=>'true');
							echo form_input($mkn_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_nama = array('name' => 'mkn_nama' , 'maxlength' => '30' ,
							'value' => $hasil->mkn_nama , 'size' => '20');
							echo form_input($mkn_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Tipe Makanan</td>
				<td>:</td>
				<td>
					<?php 
                      // $role = array('name'=>'role', 'maxlength'=>'20', 'value'=>$hasil->role, 'size'=>'20', 'class' => 'form-control');
                      // echo form_input($role);
                      $mkn_tipe = array(
                      '' => '-- Pilih Tipe Makanan --',
                      'Nabati' => 'Nabati',
                      'Hewani' => 'Hewani',
                      );
                      if($hasil->mkn_tipe == "Nabati")
                      {
                        echo form_dropdown('mkn_tipe', $mkn_tipe, 'Nabati'); 
                      }
                      else if($hasil->mkn_tipe == "Hewani")
                      {
                        echo form_dropdown('mkn_tipe', $mkn_tipe, 'Hewani'); 
                      }
                      else
                      {
                        echo form_dropdown('mkn_tipe', $mkn_tipe, '-- Pilih Tipe Makanan --');
                      }
                     ?>
					
				</td>
			</tr>
			<tr>
				<td>Kalori Makanan</td>
				<td>:</td>
				<td>
					<?php
						$mkn_kalori = array('onkeypress' => 'validAngka($this)' ,'name' => 'mkn_harga' , 'cols' => '30' , 'rows' => '3');
							echo form_input( $mkn_kalori);
					?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'simpan' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>