<script language='javascript'>
function validAngka(a)
{
	if(!/^[0-9.]+$/.test(a.value))
	{
	a.value = a.value.substring(0,a.value.length-1000);
	}
}
</script>
<html>
	<head>
		<title>Tambah minuman</title>
	</head>
	<body>
		<h3>Form Tambah minuman</h3>
		<?php
			//akses controller lalu akses fungsi tambah 
			echo form_open('MinumanController/update/'.$hasil->mnm_id);
		?>
		<table>
			<tr>
				<td>ID Minuman</td>
				<td>:</td>
				<td>
					<?php
						$mnm_id = array('name' => 'mnm_id' , 'maxlength' => '30' ,
							'value' =>$hasil->mnm_id , 'size' => '30' , 'readonly'=>'true');
							echo form_input($mnm_id);
					?>
					</td>
			</tr>
			<tr>
				<td>Nama Minuman</td>
				<td>:</td>
				<td>
					<?php
						$mnm_nama = array('name' => 'mnm_nama' , 'maxlength' => '30' ,
							'value'=>$hasil-> mnm_nama , 'size' => '20');
							echo form_input($mnm_nama);
					?>
					</td>
			</tr>
			<tr>
				<td>Kalori</td>
				<td>:</td>
				<td>
					<?php
						$mnm_kalori = array('name' => 'mnm_kalori' , 'cols' => '30' , 'rows' => '3' , 'value' =>$hasil-> mnm_kalori);
							echo form_input($mnm_kalori);
					?></td>
					
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<?php
							echo form_submit('submit' , 'simpan' , 'id="submit"');
					?>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</body>
</html>