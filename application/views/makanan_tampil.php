<html>
	<head>
		<title>Tampil Makanan</title>
	</head>
	<body>
		<h3>
			<?php
				echo anchor('MakananController/tambah' , 'Tambah Data');
			?>
		</h3>
		<h3>Data Makanan</h3>
		<table border='1' cellspacing='0' cellpadding="5">
		<tr align="center">
			<td><b>NO</b></td>
			<td><b>ID Makanan</b></td>
			<td><b>Nama Makanan</b></td>
			<td><b>Tipe Makanan</b></td>
			<td><b>Kalori Makanan</b></td>
			<td><b>Aksi</b></td>
		</tr>
		<?php
			$no=1;
			if(!empty($hasil))
			{
				foreach($hasil as $data)
				{
					?>
					<tr>
							<td> <?php echo $no; ?> </td>
							<td><?php echo $data->mkn_id; ?></td>
							<td><?php echo $data->mkn_nama; ?></td>
							<td><?php echo $data->mkn_tipe; ?> </td>
							<td>Rp.<?php echo $data->mkn_kalori; ?></td>
							<td>
								<a href='<?php echo base_url()."index.php/MakananController
								/update/".$data->mkn_id; ?>'>ubah</a>
								<a href='<?php echo base_url()."index.php/MakananController
								/delete/".$data->mkn_id; ?>'>Hapus</a>
							</td>
					</tr>
					<?php
						$no++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="5"><b> TIDAK ADA DATA </b> </td>
				</tr>
				<?php
			}
			?>
			</table>
		
	</body>
</html>