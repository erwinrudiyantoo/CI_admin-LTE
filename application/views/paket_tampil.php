<html>
	<head>
		<title>Tampil Paket</title>
	</head>
	<body>
		<h3>
			<?php
				echo anchor('PaketController/tambah' , 'Tambah Data');
			?>
		</h3>
		<h3>Data Paket</h3>
		<table border='1' cellspacing='0' cellpadding="5">
		<tr align="center">
			<td><b>NO</b></td>
			<td><b>ID Paket</b></td>
			<td><b>Nama Paket</b></td>
			<td><b>Durasi Paket</b></td>
			<td><b>Harga Paket</b></td>
			<td><b>Aksi</b></td>
		</tr>
		<?php
			$no=1;
			if(!empty($hasil))
			{
				foreach($hasil as $data)
				{
					?>
					<tr>
							<td> <?php echo $no; ?> </td>
							<td><?php echo $data->pak_id; ?></td>
							<td><?php echo $data->pak_nama; ?></td>
							<td><?php echo $data->pak_durasi; ?> hari</td>
							<td>Rp.<?php echo $data->pak_harga; ?></td>
							<td>
								<a href='<?php echo base_url()."index.php/PaketController
								/update/".$data->pak_id; ?>'>ubah</a>
								<a href='<?php echo base_url()."index.php/PaketController
								/delete/".$data->pak_id; ?>'>Hapus</a>
							</td>
					</tr>
					<?php
						$no++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="5"><b> TIDAK ADA DATA </b> </td>
				</tr>
				<?php
			}
			?>
			</table>
		
	</body>
</html>